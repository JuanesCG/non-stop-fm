<!DOCTYPE html>
<html lang="en">
<head>
    <!-- created by: Juan Esteban Castro Guerrero -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NS fm</title>    
    <link rel="stylesheet" href="styles/main.css">
    <meta name="viewport" content="width=device-width, initial-scale=1"><link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/mediaelement/2.22.0/mediaelementplayer.min.css'><link rel="stylesheet" href="styles/style.css">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script>
	$(document).ready(function(){
        // loads songs in the 'trending' section
        $.ajax({ url: "php/load_contents_trending.php",
        context: document.body,
        success: function(msg){
           //alert(msg);
           $( "#trending" ).append(msg);        
        }});
        // loads songs in the 'new' section
        $.ajax({ url: "php/load_contents_new.php",
        context: document.body,
        success: function(msg){
           //alert(msg);
           $( "#new" ).append(msg);
        }});

        // loads favorite songs for current user
        $.ajax({ url: "php/load_favorites.php",
        context: document.body,
        success: function(returned_value){
            
           if(returned_value){
                var result =jQuery.parseJSON(returned_value);
                for (let i = 0; i < result.length; ++i) {
                    var songIdTrending = '#trending> #'+result[i][1]+ "> #content> #fav";
                    //alert(songIdTrending);

                    var songIdNew = '#new> #'+result[i][1]+ "> #content> #fav";
                    //alert(songIdNew);

                    $(songIdTrending).addClass("clicked");
                    $(songIdNew).addClass("clicked");
                    
                }
           }
            
        }});

        // on click on any play button, play respective song
        $(document).on("click","button.play_button",function(){
                $('[id=play]').removeClass("clicked"); 
                var id=$(this).parent().parent().attr('id');
                $(this).toggleClass("clicked");
                var dir_data=$(this).parent().parent().attr('id');
                $.ajax({url: "php/load_song.php",
                        method: 'POST' ,
                        data: {dir: dir_data },
                        context: document.body,
                        success: function(msg){
                            
                            //alert(msg);
                            //var id=$(this).parent().parent().attr('id');
                            var songTitle =$('#'+id+ '>p>b').text();
                            $('#audio_main').trigger("pause");
                            $('#audio_main').attr('src',msg );
                            $('.bottom_msg').text(songTitle);
                            $('#audio_main').trigger("play");
                               
                 }});

        })

        // on click on any fav button, change its state in the db
        $(document).on("click","button.fav_button",function(){

                // get song_id
                var song_id=$(this).parent().parent().attr('id');

                if ($(this).hasClass("clicked")){
                    //alert("true, it's red, let's remove it");
                    $(this).toggleClass("clicked");
                    $.ajax({url: "php/remove_fav.php",
                        method: 'POST' ,
                        data: {id: song_id },
                        context: document.body,
                        success: function(msg){                            
                            //alert(msg);
                    }});
                }
                else if(!$(this).hasClass("clicked")){
                    
                    $(this).toggleClass("clicked");
                    
                    //alert("false, it's blank. Now it's red and you clicked"+song_id);
                    $.ajax({url: "php/update_fav.php",
                        method: 'POST' ,
                        data: {id: song_id },
                        context: document.body,
                        success: function(msg){                            
                            //alert(msg);
                        
                    }});

                }
        })
});
    </script>  
</head>
<body>
    <!-- Menu button -->
    <span class="btnOpt" id="btnopt" onclick="openNav()"></span>
    <!-- Sidenav -->
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <p>Hey there, <br/> <?php echo $user->getNombre(); ?> !<hr class="full">  </p>
        
        <a href="#" id="myBtn" class="menu_item"><div class="icon home"></div><p>Home</p></a>
        <a href="#"><div class="icon discover"></div><p>Discover</p></a>
        <a href="#"><div class="icon heart"></div><p>favorites</p></a>
        <a href="#"><div class="icon play"></div><p>Endless Mix</p></a>
    </div>
    <!-- Header -->
    <div class="top">
        <header>
            <a class="logo" ><img src="styles/images/logo_navbar_green.png" alt="logo"></a>
            <article>
            <div class="cont">
            <h6>Live Now!</h6>
            <time><script> document.write(new Date().toLocaleDateString()); </script></time>
            </div>
            <audio class="audio" controls="controls">
                <source src="http://localhost:8443/stream" type="audio/ogg"> 
                <!-- <source src="01 - Blew.mp3" type="audio/mp3"> -->
                <!-- <source src="" type="audio/mpeg"> -->
                Your browser does not support the audio element.
            </audio> 
            </article>
            <!-- <iframe class= "player" width="100%" height="60" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&mini=1&feed=%2FGTARadio%2Fnon-stop-pop-fm%2F" frameborder="0" ></iframe> -->
            <span class="vertical-line"></span>
            
            <nav>
                <ul class="nav__links">
                    <li><a href="#">My profile</a></li>
                    <li><a href="#">Config</a></li>
                </ul>
            </nav>
            <a class="cta" href="php/logout.php">Log out</a>
        </header>
    </div>
    <!-- Main Body -->
    <div class="main">
        
            <div class="main_title" >Trending right now</div>
                <div class="main_content_box" id="trending">
                </div>
                
            <div class="main_title" >New</div>
                <div class="main_content_box " id="new">
                </div>
            
            <div class="main_title" >Recommended for you</div>
                <div class="main_content_box" id="recommended">
                    <div class="main_content">
                        <p>Title</p>
                        <img class="img" src="styles/images/default_cover.png" alt="cover">
                        <hr>
                        <div class="content_time">00:00</div>
                        <div class="content_buttons">
                            <button id="play" class="play_button"></button>
                            <button id="fav" class="fav_button"></button>   
                        </div>
                        <div class="content_info">Hip-hop, 2020</div>
                    </div>
                    <div class="main_content">
                        <p>Title</p>
                        <img class="img" src="styles/images/default_cover.png" alt="cover">
                        <hr>
                        <div class="content_time">00:00</div>
                        <div class="content_buttons">
                            <button id="play" class="play_button"></button>
                            <button id="fav" class="fav_button"></button>   
                        </div>
                        <div class="content_info">Hip-hop, 2020</div>
                    </div>
                    <div class="main_content">
                        <p>Title</p>
                        <img class="img" src="styles/images/default_cover.png" alt="cover">
                        <hr>
                        <div class="content_time">00:00</div>
                        <div class="content_buttons">
                            <button id="play" class="play_button"></button>
                            <button id="fav" class="fav_button"></button>   
                        </div>
                        <div class="content_info">Hip-hop, 2020</div>
                    </div>
                    <div class="main_content">
                        <p>Title</p>
                        <img class="img" src="styles/images/default_cover.png" alt="cover">
                        <hr>
                        <div class="content_time">00:00</div>
                        <div class="content_buttons">
                            <button id="play" class="play_button"></button>
                            <button id="fav" class="fav_button"></button>   
                        </div>
                        <div class="content_info">Hip-hop, 2020</div>
                    </div>
                    <div class="main_content">
                        <p>Title</p>
                        <img class="img" src="styles/images/default_cover.png" alt="cover">
                        <hr>
                        <div class="content_time">00:00</div>
                        <div class="content_buttons">
                            <button id="play" class="play_button"></button>
                            <button id="fav" class="fav_button"></button>   
                        </div>
                        <div class="content_info">Hip-hop, 2020</div>
                    </div>
                </div>
    </div>
    <!-- Bottom Player -->
    <div class="bottom">
    <audio class="audio" id="audio_main" controls="controls">
                <!-- <source src="http://localhost:8443/stream" type="audio/ogg">   -->
                <!-- <source src="01 - Blew.mp3" type="audio/mp3"> -->
                <!-- <source src="" type="audio/mpeg"> -->
                Your browser does not support the audio element.
    </audio>
    </div>
    <!-- Song Name -->
    <div class="bottom_info">
        <div class="bottom_title">Now playing</div>
        <div class="bottom_msg">Song Title</div>                
    </div>

<script>
    
    $('#audio_main').attr('src','uploads/Gorillaz/The Now Now/02. Tranz.mp3' );

  // functions for sidenav
  /* Set the width of the side navigation to 280px */
  function openNav() {
      document.getElementById("mySidenav").style.width = "280px";
      $('#btnopt').fadeOut();
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
      $('#btnopt').show();
    }
    
</script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/mediaelement/2.22.0/jquery.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/mediaelement/2.22.0/mediaelement-and-player.min.js'></script><script  src="scripts/script.js"></script>

</body>
</html>