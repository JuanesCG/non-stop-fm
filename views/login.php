<!DOCTYPE html>
<html lang="en">
<head>
    <!-- created by: Juan Esteban Castro Guerrero -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NS fm</title>
    <link rel="stylesheet" href="styles/login.css">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="scripts/animate.js"></script>
</head>
<body>
    <!-- <input type="button" value="Registrarse" name ="btnRegister" id="btnRegister"> -->
    <div class="top">
    <header>
        <a class="logo" ><img src="styles/images/logo_navbar_green.png" alt="logo"></a>
        <nav>
            <ul class="nav__links">
                <li><a href="#">Pricing</a></li>
                <li><a href="#">About</a></li>
            </ul>
        </nav>
        <a class="cta" href="#">Contact</a>
        <p class="cta" id="reg">Sign up</p>
        <!-- <input class="ctaBtn" value="Sign up"
                                id ="btnReg"> -->
    </header>
    </div>
        
    <div class="main"> 
        <form class= 'form_main' action="" method="POST">
            <img src="styles/images/logo1.png" alt="Our cool logo!"
            style="width:220px;height:170px;border-bottom:solid white">
            <h2 >Log In</h2>
            <!-- <p>Username</p> <br> -->
            <input class="ctaInput" type="text" name="username" placeholder="Username" required>
            <!-- <p>Password</p> <br> -->
            <input class="ctaInput" type="password" name="password" placeholder="Password" required>
            <input class="ctaBtn" type="submit" value="Let me in!"
                                name ="btnLogin">

        </form>
    </div>

    <div id="registry" class="registry">
        <span id="close" class="closebtn float" >&times;</span> 
        <form class= 'form_reg' action="" method="POST">
        <h2 class='title'>Start broadcasting today!</h2>
            <div class="grid">
                <input class="ctaInput" type="text" name="new_name" placeholder="Names" required>
                <input class="ctaInput" type="text" name="new_surname" placeholder="Surname" required>
                <input class="ctaInput" type="email" name="new_email" placeholder="Email" required>
                <input id= "date" class="ctaInput" type="text" name="new_bd" placeholder="Birthdate" onfocus="(this.type='date')" required>
            </div>
            <hr>
            <div class="row">
                <input class="ctaInput" type="text" name="new_username" placeholder="Username" required>
                <input class="ctaInput" type="password" name="new_password" placeholder="Password" required>
                <input class="ctaInput" type="password" name="new_password_confirm" placeholder="Confirm Password" required>
            </div>
            <input class="ctaBtn green" type="submit" value="Create my account!"
                                name ="btnReg">
        </form>
    </div> 


    <div class="info_msg">
        <span class="closebtn" onclick="this.parentElement.style.display='none';" >&times;</span> 
        <div class="msg">
        
        <?php
                if(isset($errorLogin)){
                    echo $errorLogin;
                }
                else{
                    $errorLogin ="Logged out successfully";
                    echo $errorLogin;   
                }
        ?>
        </div>
    </div>

    
</body>
</html>