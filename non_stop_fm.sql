-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-07-2020 a las 18:38:53
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `non_stop_fm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `album`
--

CREATE TABLE `album` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `artist` varchar(50) NOT NULL,
  `genre` varchar(30) NOT NULL,
  `release_date` date NOT NULL,
  `image` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `album`
--

INSERT INTO `album` (`ID`, `name`, `artist`, `genre`, `release_date`, `image`) VALUES
(1, 'Meteora', 'Linkin Park', 'Alt-Metal', '2003-03-25', '..uploads/Linkin Park/Meteora.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `song`
--

CREATE TABLE `song` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `artist` varchar(50) NOT NULL,
  `length` time NOT NULL,
  `genre` varchar(30) NOT NULL,
  `release_date` date NOT NULL,
  `image` varchar(120) DEFAULT NULL,
  `file_dir` varchar(120) NOT NULL,
  `album` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `song`
--

INSERT INTO `song` (`ID`, `name`, `artist`, `length`, `genre`, `release_date`, `image`, `file_dir`, `album`) VALUES
(1, 'Foreword', 'Linkin Park', '00:16:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/01 - Foreword.mp3', 1),
(2, 'Don\'t Stay', 'Linkin Park', '03:10:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/02 - Don\'t Stay.mp3', 1),
(3, 'Somewhere I Belong', 'Linkin Park', '03:36:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/03 - Somewhere I Belong.mp3', 1),
(4, 'Lying From You', 'Linkin Park', '02:58:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/04 - Lying From You.mp3', 1),
(5, 'Hit The Floor', 'Linkin Park', '02:47:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/05 - Hit The Floor.mp3', 1),
(6, 'Easier To Run', 'Linkin Park', '03:27:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/06 - Easier To Run.mp3', 1),
(7, 'Faint', 'Linkin Park', '02:45:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/07 - Faint.mp3', 1),
(8, 'Figure.09', 'Linkin Park', '03:20:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/08 - Figure.09.mp3', 1),
(9, 'Breaking The Habit', 'Linkin Park', '03:19:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/09 - Breaking The Habit.mp3', 1),
(10, 'From The Inside', 'Linkin Park', '02:58:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/10 - From The Inside.mp3', 1),
(11, 'Nobody\'s Listening', 'Linkin Park', '03:01:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/11 - Nobody\'s Listening.mp3', 1),
(12, 'Session', 'Linkin Park', '02:27:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/12 - Session.mp3', 1),
(13, 'Numb', 'Linkin Park', '03:10:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/13 - Numb.mp3', 1),
(14, 'My December', 'Linkin Park', '04:22:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/14 - My December (Bonus Track).mp3', 1),
(15, 'Leave Out All The Rest', 'Linkin Park', '03:21:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/15 - Leave Out All The Rest (Bonus Track).mp3', 1),
(16, 'New Divide', 'Linkin Park', '04:32:00', 'Alt-Metal', '2003-03-25', 'uploads/Linkin Park/Meteora/Meteora.jpg', 'uploads/Linkin Park/Meteora/16 - New Divide (Bonus Track).mp3', 1),
(17, 'Voices', 'Disturbed', '03:11:00', 'Hard Rock', '2000-03-07', 'uploads/Disturbed/The Sickness/The Sickness.jpg', 'uploads/Disturbed/The Sickness/01 - Voices.mp3', NULL),
(18, 'John & Nancy', 'Jack Stauber', '03:31:00', 'Pop', '2018-04-14', 'uploads/Jack Stauber/HiLo/john-nancy.jpg', 'uploads/Jack Stauber/HiLo/John & Nancy.mp3', NULL),
(19, 'Tranz', 'Gorillaz', '03:31:00', 'Alternative-Indie', '2018-06-29', 'uploads/Gorillaz/The Now Now/The Now Now.jpg', 'uploads/Gorillaz/The Now Now/02. Tranz.mp3', NULL),
(20, 'Tomorrow Comes Today', 'Gorillaz', '03:13:00', 'Alternative-Indie', '2011-11-08', 'uploads/Gorillaz/The Singles Collection/cover.jpg', 'uploads/Gorillaz/The Singles Collection/01.Tomorrow Comes Today.mp3', NULL),
(21, 'Clint Eastwood', 'Gorillaz', '05:42:00', 'Alternative-Indie', '2011-11-08', 'uploads/Gorillaz/The Singles Collection/cover.jpg', 'uploads/Gorillaz/The Singles Collection/02.Clint Eastwood.mp3', NULL),
(22, 'Feel Good Inc', 'Gorillaz', '03:42:00', 'Alternative-Indie', '2011-11-08', 'uploads/Gorillaz/The Singles Collection/feel_good.jpg', 'uploads/Gorillaz/The Singles Collection/05.Feel Good Inc.mp3', NULL),
(23, 'Dare', 'Gorillaz', '03:32:00', 'Alternative-Indie', '2011-11-08', 'uploads/Gorillaz/The Singles Collection/dare.jpg', 'uploads/Gorillaz/The Singles Collection/06.Dare.mp3', NULL),
(24, 'Doncamatic', 'Gorillaz', '03:20:00', 'Alternative-Indie', '2011-11-08', 'uploads/Gorillaz/The Singles Collection/doncomatic.jpg', 'uploads/Gorillaz/The Singles Collection/13.Doncamatic.mp3', NULL),
(25, 'Victim', 'Avenged Sevenfold', '07:33:00', 'Alt-Metal', '2010-07-23', 'uploads/Avenged Sevenfold/Nightmare/cover.jpg', 'uploads/Avenged Sevenfold/Nightmare/08 - Victim.mp3', NULL),
(26, 'Natural Born Killer', 'Avenged Sevenfold', '05:19:00', 'Alt-Metal', '2010-07-23', 'uploads/Avenged Sevenfold/Nightmare/cover.jpg', 'uploads/Avenged Sevenfold/Nightmare/05 - Natural Born Killer.mp3', NULL),
(27, '21st Century Breakdown', 'Green Day', '05:10:00', 'Pop-Punk', '2009-05-15', 'uploads/Green Day/21st Century Breakdown/cover.jpg', 'uploads/Green Day/21st Century Breakdown/02 21st Century Breakdown.mp3', NULL),
(28, 'Restless Heart Syndrome', 'Green Day', '04:21:00', 'Pop-Punk', '2009-05-15', 'uploads/Green Day/21st Century Breakdown/cover.jpg', 'uploads/Green Day/21st Century Breakdown/13 Restless Heart Syndrome.mp3', NULL),
(29, 'The Grouch', 'Green Day', '02:12:00', 'Punk-Rock', '1997-10-14', 'uploads/Green Day/Nimrod/cover.jpg', 'uploads/Green Day/Nimrod/03 The Grouch.mp3', NULL),
(30, 'Redundant', 'Green Day', '03:17:00', 'Punk-Rock', '1997-10-14', 'uploads/Green Day/Nimrod/cover.jpg', 'uploads/Green Day/Nimrod/04 Redundant.mp3', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `birthdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`ID`, `name`, `surname`, `email`, `username`, `password`, `birthdate`) VALUES
(1, 'Juan', 'Castro', 'ejemplo@mail.com', 'juanecg', '$2y$10$f0y7qegA8yIDElTi3YCCheqkTD3aSI4uYnfqRb5SRh/9E.5J6D/su', '1998-11-06'),
(2, 'Pedro', 'Lopez', 'ejemplo2@gmail.com', 'pedro', '$2y$10$X8pP2fxY5HeC.fRnzmvC4ev8c8ZXgwJnlTWXDX9TdEE3CT.NPOiPe', '2000-06-03'),
(3, 'John', 'Doe', 'john@example.es', 'johnD', '$2y$10$aBaX5fcfmy75auIRqJaWh.l1Xb.MlwVbMTx0wdzuDOoNk0A6IQgu2', '1992-06-06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_liked_songs`
--

CREATE TABLE `user_liked_songs` (
  `user_id` int(11) NOT NULL,
  `liked_song_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user_liked_songs`
--

INSERT INTO `user_liked_songs` (`user_id`, `liked_song_id`) VALUES
(1, 2),
(1, 13),
(1, 17),
(1, 18),
(1, 29),
(2, 2),
(2, 13),
(3, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_uploaded_songs`
--

CREATE TABLE `user_uploaded_songs` (
  `user_id` int(11) NOT NULL,
  `uploaded_song_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `song`
--
ALTER TABLE `song`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `album` (`album`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `user_liked_songs`
--
ALTER TABLE `user_liked_songs`
  ADD PRIMARY KEY (`user_id`,`liked_song_id`),
  ADD KEY `song_id` (`liked_song_id`);

--
-- Indices de la tabla `user_uploaded_songs`
--
ALTER TABLE `user_uploaded_songs`
  ADD PRIMARY KEY (`user_id`,`uploaded_song_id`),
  ADD KEY `song_id` (`uploaded_song_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `album`
--
ALTER TABLE `album`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `song`
--
ALTER TABLE `song`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `song`
--
ALTER TABLE `song`
  ADD CONSTRAINT `song_ibfk_1` FOREIGN KEY (`album`) REFERENCES `album` (`ID`);

--
-- Filtros para la tabla `user_liked_songs`
--
ALTER TABLE `user_liked_songs`
  ADD CONSTRAINT `user_liked_songs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`ID`),
  ADD CONSTRAINT `user_liked_songs_ibfk_2` FOREIGN KEY (`liked_song_id`) REFERENCES `song` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user_uploaded_songs`
--
ALTER TABLE `user_uploaded_songs`
  ADD CONSTRAINT `user_uploaded_songs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_uploaded_songs_ibfk_2` FOREIGN KEY (`uploaded_song_id`) REFERENCES `song` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
