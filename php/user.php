<?php
include 'db.php';

class User extends DB{
    private $nombre;
    private $username;
    private $userID;

    // functions that have to do with database usage

    public function getFavs($user){

        // retrieves all favorites stored inside the database for a given user
        $query = $this->connect()->prepare("SELECT * FROM user_liked_songs WHERE user_liked_songs.user_id=:user");
        $query->execute(['user'=>$user]);
        // try to search for fav's, if user doesn't have any return False
        if($query->rowCount()){
            $result = $query->fetchAll();
            return $result;
        }else{
            return false;
        }

    }

    public function removeFav($user,$song){

        //removes a favorite song from a given user's favorite list
        $query = $this->connect()->prepare("DELETE FROM `user_liked_songs` WHERE user_liked_songs.user_id=:user and user_liked_songs.liked_song_id=:song");
         
        // try to remove fav, if execution fails, it's an error and it must be caught
        try {
            $query->execute(['user'=>$user,'song'=>$song]);
            return true;
        } catch(PDOException $e) {
            echo $e->getMessage();
            return false;
        }
        
    }

    public function addFav($user, $song){

        //adds a favorite song to a given user's favorite list
        $query = $this->connect()->prepare("INSERT INTO user_liked_songs (user_liked_songs.user_id,user_liked_songs.liked_song_id) VALUES (:user,:song)");

        // try to add fav, if it already is added it's an error and it must be caught
        try {
            $query->execute(['user'=>$user,'song'=>$song]);
            return true;
        } catch(PDOException $e) {
            echo $e->getMessage();
            return false;
        }
        
    }

    public function getSongByID($ID){


        // retrieves a song by it's ID
        $query = $this->connect()->prepare("SELECT song.file_dir FROM song WHERE song.ID=:song_id");
        $query->execute(['song_id' => $ID]);

        if($query->rowCount()){
            $result = $query->fetch();
            return $result['file_dir'];
        }else{
            return false;
        }
    }


    public function trendingSongs(){
        
        // Trending: songs that have been liked at least once
        // should add a limit when there are too much songs
        $query = $this->connect()->prepare("SELECT *, count(*) as ocurrences from song join user_liked_songs on song.ID=user_liked_songs.liked_song_id GROUP by song.ID order by ocurrences DESC ");
        $query->execute();

        if($query->rowCount()){
            $result = $query->fetchAll();
            return $result;
        }else{
            return false;
        }

    }
    public function newSongs(){

        // New: songs that were added recently

        $query = $this->connect()->prepare("SELECT * from song order by song.release_date DESC LIMIT 15 ");
        $query->execute();

        if($query->rowCount()){
            $result = $query->fetchAll();
            return $result;
        }else{
            return false;
        }
    }
    // public function recommendedSongs{}

    public function userExists($user, $pass){
        // checks if a given user exists

        //$hash = password_hash($pass, PASSWORD_DEFAULT);
        //$this is a reference to the calling object e.g the database
        $query = $this->connect()->prepare('SELECT * FROM user WHERE username = :user_n'); //AND password = :pass');
        $query->execute(['user_n' => $user]);//, 'pass' => $hash]);       

        if($query->rowCount()){
            $result = $query->fetch();
            if (password_verify($pass, $result['password'])){
                return true;
            }
        }else{
            return false;
        }
    }

    public function createUser($data){
        // creates a new user and adds it to the database

        $query = $this->connect()->prepare('SELECT * FROM user WHERE username = :user_n');
        $query->execute(['user_n' => $data[2]]);

        if($query->rowCount()>=1){
            return False;
        }

        $query = $this->connect()->prepare("INSERT INTO user (name,surname,email,username, password, birthdate) 
                                            VALUES ( '$data[0]', '$data[1]', '$data[2]', '$data[3]', '$data[4]', '$data[5]' )");
        $query->execute();
        return True;
    }

    // functions that have to do with current user

    public function changePwd($pwd){
        // changes current user password

        $hash = password_hash($pwd, PASSWORD_DEFAULT);
        $user = $this->username;
        $query = $this->connect()->prepare('UPDATE user SET password = :pass WHERE user.username = :user_n ');
        $query->execute(['pass'=> $hash, 'user_n'=> $user]);
    }


    public function setUser($user){
        // finds user and sets $nombre and $username;
        $query = $this->connect()->prepare('SELECT * FROM user WHERE username = :user_n');
        $query->execute(['user_n' => $user]);
        
        foreach ($query as $currentUser) {
            $this->nombre = $currentUser['name'];
            $this->username = $currentUser['username'];
            $this->userID = $currentUser['ID'];
        }
    }

    public function getNombre(){
        return $this->nombre;
    }
    public function getUsername(){
        return $this->username;
    }
    public function getUserID(){
        return $this->userID;
    }
}

?>