<?php
     
    include 'user.php';
    
    $user = new User();

    $result_array = $user->trendingSongs();
    
    foreach ($result_array as $row){
        // print out the final html that goes in main_content_box: trending
        $rest = substr($row['length'], 0, -3);
        echo '
        <div class="main_content" id="'.$row['ID'].'" >
            <p><b>'.$row['name'].'</b></p>
            <img class="img" src="'.$row['image'].'" alt="cover">
            <hr>
            <div class="content_time">'.$rest.'</div>
            <div id="content" class="content_buttons">
                <button id="play" class="play_button"></button>
                <button id="fav" class="fav_button"></button>   
            </div>
            <div class="content_artist">'.$row['artist'].'</div>
            <div class="content_info">'.$row['genre'].', '.$row['release_date'].'</div>
    </div>
        
        ';
    }
    
    //echo json_encode( $result_array );
    // print_r($x);

?>