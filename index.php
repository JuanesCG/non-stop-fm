<?php

include_once 'php/user.php';
include_once 'php/user_session.php';

$userSession = new UserSession();
$user = new User();
$result = "";

if (isset($_POST['btnReg'])) {

    // on btnReg pressed, validate data, create user and redirect to login.php

    //retrieve data from form 
    $n_name = $_POST['new_name']; 
    $n_surname = $_POST['new_surname']; 
    $n_email = $_POST['new_email']; 
    $n_username = $_POST['new_username']; 
    $n_password = $_POST['new_password']; 
    $confirm_pwd = $_POST['new_password_confirm'];
    $birthdate = $_POST['new_bd'];

    // validate that no var is empty

    //validate password
    if ($n_password != $confirm_pwd){
        $errorLogin = "¡Passwords do not match!";
        include_once 'views/login.php';
    }
    //password is correct, try to create user
    else{
        // ecrypt password on a 60 pos. char!
        $hash = password_hash($n_password, PASSWORD_DEFAULT);
        $data = array( $n_name,$n_surname,$n_email,$n_username,$hash,$birthdate);

        // if everythin went ok, inform the user and redirect to login.php
        if($user->createUser($data)){
            $errorLogin = "You've been correctly registrated!";
            include_once 'views/login.php';
        }
        // if user already exists, abort operation
        else{
            $errorLogin = "Error! User is already registered";
            include_once 'views/login.php';
        }
    }
}else if(isset($_SESSION['user'])){
    // if there's a session, redirect to main.php 

    $user->setUser($userSession->getCurrentUser());
    include_once 'views/main.php';

}else if(isset($_POST['username']) && isset($_POST['password'])){
    // if the username and password are set, validate them and create
    // session for current user

    // retrieve username and password from the form 
    $userForm = $_POST['username'];
    $passForm = $_POST['password'];

    if ($userForm =='' or $passForm == ''){
        $errorLogin = "Wrong Username and/or password!";
        include_once 'views/login.php';
    }
    
    // check if user exists
    else if($user->userExists($userForm, $passForm)){
        $userSession->setCurrentUser($userForm);
        $user->setUser($userForm);
        include_once 'views/main.php';
    }else{
        // name and/or pwd is incorrect, redirect to login.php
        $errorLogin = "Wrong Username and/or password!";
        include_once 'views/login.php';
    }

}else{
    // default option, if no data is given by user, simply redirect to login
    include_once 'views/login.php';
}

?>