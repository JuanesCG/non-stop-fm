$(document).ready(function(){

    $('#reg').on('click',animateLeft);
    function  animateLeft() {
        // unbind sign up btn
        $('#reg').off('click',animateLeft);
        // animate main form to the left
        $('.main').animate({
            marginLeft: "-=600px",
        }, 500);
        // show registry form
        $(".registry").addClass("show");
      }

    $('#close').click(function(){
        // animate main form to the right
        $('.main').animate({
            marginLeft: "+=600px",
        }, 500);
        // bind sign up btn
        $('#reg').on('click',animateLeft);
        // hide registry form
        $(".registry").removeClass("show");
        // change date type to show placeholder
        $('#date').prop('type', 'text');
    })

   
});


